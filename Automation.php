<?php

function copiarArchivos($origen, $destino) {
    try {
        if (!file_exists($destino)) {
            mkdir($destino, 0777, true);
        }
        $archivos = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($origen, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::SELF_FIRST
        );
        foreach ($archivos as $archivo) {
            $dest = $destino . '/' . $archivos->getSubPathName();
            if ($archivo->isDir()) {
                if (!file_exists($dest)) {
                    mkdir($dest);
                }
            } else {
                copy($archivo, $dest);
            }
        }

        echo "La operación de copia fue exitosa.\n";
    } catch (Exception $e) {
        echo "Error al copiar archivos: " . $e->getMessage() . "\n";
    }
}

copiarArchivos("/ruta/de/origen", "/ruta/de/destino");

?>