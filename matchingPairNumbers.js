function findMatchingPair(numbers, targetSum) {
    const numCount = numbers.length;

    for (let i = 0; i < numCount; i++) {
        for (let j = i + 1; j < numCount; j++) {
            const currentSum = numbers[i] + numbers[j];

            if (currentSum === targetSum) {
                return { hasMatchingPair: true, matchingPair: [numbers[i], numbers[j]] };
            }

            // Si la suma actual es mayor que el objetivo, como los números están en orden ascendente,
            // no hay necesidad de seguir buscando en esta iteración interna.
            if (currentSum > targetSum) {
                break;
            }
        }
    }

    return { hasMatchingPair: false, matchingPair: null };
}

// Ejemplo de uso
const numbers1 = [2, 3, 6, 7];
const sum1 = 9;
const result1 = findMatchingPair(numbers1, sum1);
console.log(result1);